# OpenML dataset: WorkersCompensation

https://www.openml.org/d/42876

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset describes 100,000 realistic, synthetically generated worker compensation insurance claims. Along the ultimate financial losses, each claim is described by the initial case estimate, date of accident and reporting date, a text describing the accident and demographic info on the worker.

The dataset was kindly created and provided by Colin Priest. While similar, it is not identical to the dataset used in www.kaggle.com/c/actuarial-loss-estimation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42876) of an [OpenML dataset](https://www.openml.org/d/42876). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42876/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42876/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42876/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

